TicTacToe
=========

Tic Tac Toe program written in Go.

How to Play
-----

1. Install [Go](https://golang.org/doc/install).
2. `git clone git@bitbucket.org:jdsung/go-ttt.git`
3. Change directory into the root directory of the repository.
4. `./play_game`

How to Run Tests
-----

1. `cd tictactoe` within the root directory.
2. `go get github.com/stretchr/testify`
3. `go test`
